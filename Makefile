DOCKER_ID = $$(docker ps -a -f name=e2e_test --format {{.ID}})
DOCKER_IMAGE = 2heoh/blink

build:
	./gradlew build
	docker build --build-arg JAR_FILE=build/libs/*.jar -t $(DOCKER_IMAGE) .
	echo "$(DOCKER_HUB_PASSWORD)" | docker login -u 2heoh --password-stdin
	docker push $(DOCKER_IMAGE)

test:
	make docker-clean-test
	if [ -z "$(APP_HOST)" ]; then echo "ERROR: APP_HOST is not set" && false; fi
	if [ -z "$(APP_PORT)" ]; then echo "ERROR: APP_PORT is not set" && false; fi
	docker pull $(DOCKER_IMAGE)
	docker run -d -p $(APP_PORT):8080 --name e2e_test -t $(DOCKER_IMAGE)
	./check_service.sh http://$(APP_HOST):$(APP_PORT)
	./gradlew e2eTest -i --rerun-tasks
	make docker-clean-test

docker-clean-test:
	if [ ! -z "$(DOCKER_ID)" ]; then docker stop $(DOCKER_ID) || true && docker rm $(DOCKER_ID) || true; fi

install:
	./install.sh
