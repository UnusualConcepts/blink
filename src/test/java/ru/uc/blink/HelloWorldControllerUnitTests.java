package ru.uc.blink;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HelloWorldControllerUnitTests {

	@Autowired
	private HelloWorldController controller;

	@Test
	public void indexShouldReturnIndexTemplate() {
		FakeModel model = new FakeModel();

        String output = controller.index(model);

        assertEquals(output, "index");
    }

}

