package ru.uc.blink;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloWorldController {

    @RequestMapping("/")
    public String index(Model model) {

        model.addAttribute("message","blink подмигивает тебе ;)");

        return "index";
    }
}
