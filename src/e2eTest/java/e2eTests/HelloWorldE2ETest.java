package e2eTests;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HelloWorldE2ETest {

    private URL url;

    @Before
    public void setUp() throws Exception {
        String appAddress = "http://" + System.getenv("APP_HOST") + ":" + System.getenv("APP_PORT");
        System.out.println("Generated APP address: " + appAddress);
        url = new URL(appAddress);
    }

    @Test
    public void shouldDisplayHelloMessage() throws Exception {

        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");


        String output = readBody(con);

        Assert.assertThat(output, CoreMatchers.containsString("blink подмигивает тебе ;)"));
    }

    private String readBody(HttpURLConnection con) throws IOException {
        StringBuilder sb = new StringBuilder();
        int c;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
        while ( (c =bufferedReader.read()) != -1 ) {
            sb.append((char) c);
        }

        return sb.toString();
    }
}